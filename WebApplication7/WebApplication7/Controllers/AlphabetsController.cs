﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication7.Models;

namespace WebApplication7.Controllers
{
    
  
    public class AlphabetsController : ControllerBase
    {

        [HttpPost, Route("api/alphabetsClass")]
        public ActionResult Vowels(AlphabetsClass checkvowels)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? name = checkvowels.Name;
                bool result = Isvowel(name); 
                return Ok(result ? "contain Vowel's" : "Does not contain vowel's");

            }
            catch (Exception)
            {
                throw;
            }

        }
        static bool Isvowel(string name)
        {
            name = name.ToLower();
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] == 'a' || name[i] == 'e' || name[i] == 'i' || name[i] == 'o' || name[i] == 'u')
                    return true;
            }
            return false;
        }
    }

    public class BoxUnboxController : ControllerBase
    {
        [HttpPost, Route("api/BoxUnboxClass")]
        public ActionResult BoxUnbox(BoxUnbox boxUnbox)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? number = boxUnbox.Number;
                for (int i = 0; i < number?.Length - 1; i++)
                {
                    object first = i;
                    object second = i + 1;
                    SwapNumbers(number, first, second);
                }

                return Ok(number);
            }
            catch (Exception)
            {
                throw;
            }
        }
        static void SwapNumbers(int[] array, object first, object second)
        {
            int input1 = (int)first;
            int input2 = (int)second;
            (array[input1], array[input2]) = (array[input2], array[input1]);
        }

    }

    public class CheckAnagramController : ControllerBase
    {
        [HttpPost, Route("api/AnagramClass")]
        public ActionResult Anagram(AnagramClass anagramString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string inputString1 = anagramString.Name1;
                string inputString2 = anagramString.Name2;

                inputString1.ToLower();
                inputString2.ToLower();

                if (inputString1.Length == inputString2.Length)
                {
                    char[] str1charArray = inputString1.ToCharArray();
                    char[] str2charArray = inputString2.ToCharArray();

                    Array.Sort(str1charArray);
                    Array.Sort(str2charArray);

                    bool result = Array.Equals(str1charArray, str2charArray);
                    if (result)
                    {
                        return Ok("string is anagram");
                    }

                    else
                    {
                        return Ok("string is not anagram");
                    }

                }
                else
                    return Ok("string is not anagram");

            }
            catch (Exception)
            {
                throw;
            }
        }
    }


    public class ConvertTempretureController : ControllerBase
    {
        [HttpPost, Route("api/ConvertingTempreture")]
        public ActionResult ConvertTempreture(ConvertTempretureClass ConvertTempreture)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                double inputKelvin = ConvertTempreture.Kelvin;
                double inputFahrenheit = ConvertTempreture.Fahrenheit;

                double[] result1 = Tempreture(inputKelvin);
                return Ok(result1);

                //double[] result2 = Tempreture(inputFahrenheit);
                //return Ok(result1);

            }

            catch (Exception)
            {
                throw;
            }
        }


        static double[] Tempreture(double celsius)
        {
            double Kelvin = celsius + 273.15D;
            double Fahrenheit = (celsius * 1.80) + 32D;

            double[] output = new double[2];
            output[0] = Math.Round(Kelvin, 5);
            output[1] = Math.Round(Fahrenheit, 5);
            return output;
        }
    }

    public class NullCoalescingController : ControllerBase
    {
        [HttpPost, Route("api/NullCoalescingClass")]
        public ActionResult NullColescingOperator(NullCoalescing nullCoalescing)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }


                string? userInput = nullCoalescing.Input ?? "default";
                return Ok(userInput);

            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class ShortestWordController : ControllerBase
    {
        [HttpPost, Route("api/ShortestWord")]
        public ActionResult ShortestWordDemo(ShortestWord shortestWord)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? inputName = shortestWord.Name;

                return Ok(FindShort(inputName));
            }
            catch (Exception)
            {
                throw;
            }
        }

        static int FindShort(string name)
        {
            string[] words = name.Split(' ');
            int lowest = words[0].Length;

            foreach (string word in words)
            {
                if (words.Length > lowest)
                {
                    lowest = words.Length;
                }

            }
            return lowest;

        }
    }

    public class TernaryController : ControllerBase
    {
        [HttpPost, Route("api/TernaryOperator")]
        public ActionResult Ternary(TernaryClass ternary)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int userInput = ternary.Number;
                bool isEven;
                isEven = (userInput % 2 == 0) ? true : false;
                return Ok(isEven);

            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}



