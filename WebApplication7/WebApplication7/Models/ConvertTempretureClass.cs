﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication7.Models
{
    public class ConvertTempretureClass
    {
        [Required]
        public double Kelvin { get; set; }
        public double Fahrenheit { get; set; }

    }
}
