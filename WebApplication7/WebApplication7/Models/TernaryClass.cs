﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication7.Models
{
    public class TernaryClass
    {
        [Required]
        public int Number { get; set; }
    }
}
