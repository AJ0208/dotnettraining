﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication7.Models
{
    public class ShortestWord
    {
        [Required] 
        public string? Name { get; set; }
    }
}
