﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication7.Models
{
    public class BoxUnbox
    {
        [Required]
        public int[]? Number { get; set; }
    }
}
