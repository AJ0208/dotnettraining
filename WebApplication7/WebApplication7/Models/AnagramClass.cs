﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication7.Models
{
    public class AnagramClass
    {
        [Required]
        public string Name1 { get; set; }
        public string Name2 { get; set; }
    }
}
