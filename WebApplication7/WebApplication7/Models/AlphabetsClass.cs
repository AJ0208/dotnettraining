﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication7.Models
{
    public class AlphabetsClass
    {
        [Required]
        public string? Name { get; set; }
    }
}
