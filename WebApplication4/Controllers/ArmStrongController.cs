﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class ArmStrongController : ControllerBase
    {
        [HttpPost, Route('api/ArmStrongNumber')]
        public ActionResult ArmstrongNo(Models.ArmstrongClass ArmstrongDemo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int numberInput = ArmstrongDemo.number;
                    int originalNo = numberInput;
                    int remainder = 0;
                    int result = 0;

                    while (originalNo != 0)
                    {
                        remainder = originalNo % 10;
                        result += remainder * remainder * remainder;
                        originalNo /= 10;
                    }
                    if (result == numberInput)
                    {
                        return Ok("number is armstrong number");
                    }
                    else
                    {
                        return Ok("number is not armstrong number");
                    }

                }
            }
            catch (Exception) { throw; }
        }
    }

    public class CheckNoController : ControllerBase
    {
        [HttpPost, Route("api/ClassForCheckNo")]
        public ActionResult CheckNumber(Models.ClassForCheckNo CheckNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    if (CheckNumber.number < 0)
                    {
                        return Ok("number is negative");
                    }
                    else if (CheckNumber.number > 0)
                    {
                        return Ok("number is positive");
                    }
                    else
                    {
                        return Ok("number is zero");
                    }
                }
            }
            catch (Exception) { throw; }
        }
    }

    public class DowhileController : ControllerBase
    {

        [HttpPost, Route("api/DowhileClass")]
        public ActionResult doWhilePrac(Dowhile dowhile)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }


                int userInput = dowhile.Number;
                do
                {
                    userInput = userInput + 10;

                }
                while (userInput <= 10);
                return Ok(userInput);


            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class EvenOddController : ControllerBase
    {
        [HttpPost, Route("api/EvenOdd")]
        public ActionResult EvenOddNo(Models.EvenOdd EvenOddNumber, int userInput)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {

                    int numberInput = EvenOddNumber.number;

                    string result = (numberInput % 2) == 0 ? "even number" : "odd number";
                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class FactorialNoController : ControllerBase
    {
        [HttpPost, Route("api/FactorialClass")]
        public ActionResult factorialNo(FactorialClass FactorialNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int numberInput = FactorialNumber.number;
                    int factorial = 1;

                    for (int i = 1; i <= numberInput; i++)
                    {
                        factorial *= i;
                    }
                    return Ok(factorial);

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class FebonacciSeriesController : ControllerBase
    {

        [HttpPost, Route("api/FenonacciClass")]
        public ActionResult FenonacciNo(SwappingNoClass Fenonacci)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int numberInput1 = Fenonacci.number1;
                    int numberInput2 = Fenonacci.number2;
                    int conditinalNo = 5;
                    int sum = 0;

                    for (int i = 0; i < conditinalNo; i++)
                    {

                        sum = numberInput1 + numberInput2;
                        numberInput1 = numberInput2;
                        numberInput2 = sum;


                    }
                    return Ok(numberInput2);

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class LargestNoController : ControllerBase
    {
        [HttpPost, Route("api/LargestNoClass")]
        public ActionResult LargestNo(LargestNoClass Largest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? inputArray = Largest.Array;
                    int maximumNo = inputArray[0];
                    for (int i = 1; i < inputArray.Length; i++)
                    {
                        if (inputArray[i] > maximumNo)
                        {
                            maximumNo = inputArray[i];
                        }
                    }
                    return Ok(maximumNo);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class PalindromeController : ControllerBase
    {
        [HttpPost, Route("api/PalindromeClass")]
        public ActionResult palindromeNo(PalindromeClass PalindromeDemo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int numberInput = PalindromeDemo.number;
                    int originalNo = numberInput;
                    int remainder = 0;
                    int result = 0;

                    while (originalNo != 0)
                    {
                        remainder = originalNo % 10;
                        result = result * 10 + remainder;
                        originalNo = originalNo / 10;
                    }
                    if (result == numberInput)
                    {
                        return Ok("number is Palindrome number");
                    }
                    else
                    {
                        return Ok("number is not Palindrome number");
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class PrimeNoController : ControllerBase
    {
        [HttpPost, Route("api/PrimeNo")]
        public ActionResult Primenum(PrimeNo PrimeNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int num = PrimeNumber.number;

                    for (int i = 1; i < num / 2; i++)
                    {
                        if (num % i == 0)
                        {
                            return Ok("not a prime no");
                        }
                    }
                    return Ok("prime no");


                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class SumOfNoController : ControllerBase
    {
        [HttpPost, Route("api/SumNoClass")]
        public ActionResult Sumno(SumNoClass SumDemo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int numberInput = SumDemo.number;
                    int remainder = 0;
                    int result = 0;
                    int sum = 0;

                    while (numberInput != 0)
                    {
                        remainder = numberInput % 10;
                        sum = sum + remainder;
                        numberInput = numberInput / 10;

                    }

                    return Ok(sum);


                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class SwappingNoController : ControllerBase
    {
        [HttpPost, Route("api/SwappingNoClass")]
        public ActionResult SwapNo(Models.SwappingNoClass swapping)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int inputNumber1 = swapping.number1;
                    int inputNumber2 = swapping.number2;

                    inputNumber1 = inputNumber1 + inputNumber2;
                    inputNumber2 = inputNumber1 - inputNumber2;
                    inputNumber1 = inputNumber1 - inputNumber2;
                    return Ok(new { inputNumber1, inputNumber2 });

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class SwitchCaseController : ControllerBase
    {
        [HttpPost, Route("api/SwitchClass")]
        public ActionResult SwitchNo(Models.SwitchClass switchDemo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    switch (switchDemo.day)
                    {
                        case 1: return Ok("sunday");


                        case 2: return Ok("monday");


                        case 3: return Ok("tuesday");


                        case 4: return Ok("wednesday");


                        case 5: return Ok("thursday");


                        case 6: return Ok("friday");


                        case 7: return Ok("saturday");

                        default: return Ok("Looking forward to the Weekend");

                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}

