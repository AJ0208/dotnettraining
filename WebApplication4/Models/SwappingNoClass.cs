﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Models
{
    public class SwappingNoClass
    {
        [Required]
        public int number1 { get; set; }
        public int number2 { get; set; }

    }
}
