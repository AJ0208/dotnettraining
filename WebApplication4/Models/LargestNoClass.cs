﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Models
{
    public class LargestNoClass
    {
        [Required]
        public int[]? Array { get; set; }
    }
}
