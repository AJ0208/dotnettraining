﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Models
{
    public class PrimeNo
    {
        [Required]
        public int number { get; set; }
    }
}
