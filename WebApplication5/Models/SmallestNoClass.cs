﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication5.Models
{
    public class SmallestNoClass
    {
        [Required]
        public int[] Array { get; set; }
    }
}
