﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication5.Models
{
    public class ReverseStringClass
    {
        [Required]
        public string Name { get; set; }
    }
}
