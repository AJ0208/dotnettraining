﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication5.Models
{
    public class PalindromeStringClass
    {
        [Required]
        public string Name { get; set; }
    }
}
