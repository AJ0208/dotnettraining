﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication5.Models
{
    public class DuplicateElementClass
    {
        [Required]
        public int[]? Array { get; set; }
    }
}
