﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class DuplicateElementArrayController : ControllerBase
    {
        [HttpPost, Route("api/DuplicatelementClass")]
        public ActionResult Duplicate(DuplicatelementClass dulicateDemo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? inputArray = dulicateDemo.Array;
          

                    for (int i = 0; i < inputArray.Length; i++)
                    {
                        for (int j = i + 1; j < inputArray.Length; j++)
                        {
                            if (inputArray[i] == inputArray[j])
                            {
                                return Ok(inputArray[i]);
                            }


                        }
                    }

                    return Ok("element found");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }


    public class PalindromeStringController : ControllerBase
    {
        [HttpPost, Route("api/PalindromeStringClass")]
        public ActionResult palindromeString(PalindromeStringClass Palindrome)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string inputName = Palindrome.Name;
                    string originalString = inputName;
                    int stringLength = inputName.Length;

                    string reverseString = "";
                    char[] chars = inputName.ToCharArray();

                    for (int i = stringLength - 1; i >= 0; i--)
                    {
                        reverseString = reverseString + chars[i];
                    }
                    if (originalString == reverseString)
                    {
                        return Ok("string is palindrom");

                    }
                    return Ok("string is not palindrom");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class ReverseStringController : ControllerBase
    {
        [HttpPost, Route("api/ReverseStringClass")]
        public ActionResult reverseString(ReverseStringClass ReverseNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string inputName = ReverseNumber.Name;
                    int stringLength = inputName.Length;

                    string reverseString = "";
                    char[] chars = inputName.ToCharArray();

                    for (int i = stringLength - 1; i >= 0; i--)
                    {
                        reverseString = reverseString + chars[i];
                    }
                    return Ok(reverseString);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class SmallestNoController : ControllerBase
    {

        [HttpPost, Route("api/SmallestNoClass")]
        public ActionResult smallestNumber(SmallestNoClass smallestNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[] inputArray = smallestNumber.Array;
                    int minimumNumber = inputArray[0];
                    for (int i = 1; i < inputArray.Length; i++)
                    {
                        if (inputArray[i] < minimumNumber)
                        {
                            minimumNumber = inputArray[i];
                        }
                    }
                    return Ok(minimumNumber);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class LargestNoController : ControllerBase
    {
        [HttpPost, Route("api/LargestNoClass")]
        public ActionResult largestNumber(LargestNoClass LargestNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[] inputArray = LargestNumber.Array;
                    int maximumNo = inputArray[0];
                    for (int i = 1; i < inputArray.Length; i++)
                    {
                        if (inputArray[i] > maximumNo)
                        {
                            maximumNo = inputArray[i];
                        }
                    }
                    return Ok(maximumNo);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

