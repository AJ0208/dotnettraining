﻿using BasicImplementationTask.Models.ModelsWhileLoop;
using BasicImplementationTask.ModelsLogical;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicImplementationTask.Controllers
{


    public class WhileLoopController : ControllerBase
    {
        [HttpPost , Route("api/armstrongNumber")]
        public ActionResult ArmstrongLogic(WhileLoop1 armstrongNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                
                    int numberInput = armstrongNumber.Number;
                    int originalNo = numberInput;
                    int remainder = 0;
                    int result = 0;

                    while (originalNo != 0)
                    {
                        remainder = originalNo % 10;
                        result = result + remainder * remainder * remainder;
                        originalNo = originalNo / 10;
                    }

                if (result == numberInput)
                {
                    return Ok("number is armstrong number");
                }
                else
                {
                    return Ok("number is not armstrong number");
                }
                
            }
            catch (Exception)
            { 
                throw;
            }
        }


        [HttpPost, Route("api/palindromeNumber")]
        public ActionResult PalindromeLogic(WhileLoop2 palindromeNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int numberInput = palindromeNumber.Number;
                int originalNo = numberInput;
                int remainder = 0;
                int result = 0;

                while (originalNo != 0)
                {
                    remainder = originalNo % 10;
                    result = result * 10 + remainder;
                    originalNo = originalNo / 10;
                }
                if (result == numberInput)
                {
                    return Ok("number is Palindrome number");
                }
                else
                {
                    return Ok("number is not Palindrome number");
                }
            }
            catch (Exception) 
            {
                throw;
            }
        }




    }
 }

