﻿
using BasicImplementationTask.Models.ModelsForLoopDemo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicImplementationTask.Controllers
{ 
    public class ForLoopController : ControllerBase
    {
        [HttpPost , Route("api/reverseString")]

        public ActionResult ReverseStringLogic(ForLoopDemo1 reverseString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
              
                    string? inputName = reverseString.Name;
                    int stringLength = inputName.Length;

                    string reverseStringEx = "";
                    char[] chars = inputName.ToCharArray();

                    for (int i = stringLength - 1; i >= 0; i--)
                    {
                    reverseStringEx = reverseStringEx + chars[i];
                    }
                    return Ok(reverseStringEx);
                
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
 }
