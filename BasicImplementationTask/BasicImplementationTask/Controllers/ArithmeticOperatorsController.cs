﻿using BasicImplementationTask.Models.ModelsArihmetic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace BasicImplementationTask.Controllers
{
    public class ArithmeticOperatorsController : ControllerBase
    {
        [HttpPost, Route("api/sumOfNumber")]
        public ActionResult SumOfNumbersLogic(ArithmeticDemo1 sumOfNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                
               
                    int numberInput = sumOfNumber.Number;
                    int remainder = 0;
                    int sum = 0;

                    while (numberInput != 0)
                    {
                        remainder = numberInput % 10;
                        sum = sum + remainder;
                        numberInput = numberInput / 10;

                    }

                    return Ok(sum);
                
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost, Route("api/swapNumber")]
        public ActionResult SwapNumberLogic(ArithmeticDemo2 swappingNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
               
                    int inputNumber1 = swappingNumber.Number1;
                    int inputNumber2 = swappingNumber.Number2;

                    inputNumber1 = inputNumber1 + inputNumber2;
                    inputNumber2 = inputNumber1 - inputNumber2;
                    inputNumber1 = inputNumber1 - inputNumber2;
                    return Ok(new { inputNumber1, inputNumber2 });
                
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost, Route("api/factorialNumber")]
        public ActionResult FactorialNumberLogic(ArithmeticDemo3 factorialNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
               
                    int number = factorialNumber.Number;
                    int numberInput = number;
                    int factorial = 1;

                    for (int i = 1; i <= numberInput; i++)
                    {
                        factorial *= i;
                    }
                    return Ok(factorial);
                
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost, Route("api/fibonacciSeries")]
        public ActionResult FibonacciSeriesLogic(ArithmeticDemo4 fibonacciSeries) 
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
               
                    int numberInput1 = fibonacciSeries.Number1;
                    int numberInput2 = fibonacciSeries.Number2;
                    int conditinalNo = 5;
                    int sum = 0;

                    for (int i = 0; i < conditinalNo; i++)
                    {

                        sum = numberInput1 + numberInput2;
                        numberInput1 = numberInput2;
                        numberInput2 = sum;


                    }
                   return Ok(numberInput2);
                
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost, Route("api/calculatingPersonAge")]
        public ActionResult CalculatingAgeLogic(ArithmeticDemo5 calculatingAge)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
          
                

                DateTime birthdate = calculatingAge.BirthDate;
                DateTime today = DateTime.Today;
                int age = today.Year - birthdate.Year;
                if (birthdate > today.AddYears(-age)) age--;

                return Ok(age);
            }
            catch (Exception)
            {
                throw;
            }
        }






    }
}

