﻿using BasicImplementationTask.Models.ModelsIf_Else;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicImplementationTask.Controllers
{
    public class IF_ElseController : ControllerBase
    {
        [HttpPost, Route("api/checkNumber")]
        public ActionResult CheckNumberLogic(If_Else1 checkNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    if (checkNumber.Number < 0)
                    {
                        return Ok("number is negative");
                    }
                    else if (checkNumber.Number > 0)
                    {
                        return Ok("number is positive");
                    }
                    else
                    {
                        return Ok("number is zero");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
