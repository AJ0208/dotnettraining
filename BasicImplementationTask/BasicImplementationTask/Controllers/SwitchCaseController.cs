﻿using BasicImplementationTask.Models.ModelsSwitchCase;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicImplementationTask.Controllers
{
    public class SwitchCaseController : ControllerBase
    {
        [HttpPost , Route("api/switchcaseWeak")]
        public ActionResult SwitchCaseWeaks(SwitchCase1 switchcaseWeak)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    switch (switchcaseWeak.Day)
                    {
                        case 1: return Ok("sunday");


                        case 2: return Ok("monday");


                        case 3: return Ok("tuesday");


                        case 4: return Ok("wednesday");


                        case 5: return Ok("thursday");


                        case 6: return Ok("friday");


                        case 7: return Ok("saturday");

                        default: return Ok("Looking forward to the Weekend");

                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }



    }
        
}

