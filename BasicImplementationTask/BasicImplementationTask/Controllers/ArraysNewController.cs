﻿using BasicImplementationTask.Models.ModelsArrays;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicImplementationTask.Controllers
{


    public class ArraysNewController : ControllerBase
    {
        [HttpPost, Route("api/largestNumber")]
        public ActionResult LargesetNumberLogic(ArraysDemo1 largestNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[] inputArray = largestNumber.Array;
                    int maximumNo = inputArray[0];
                    for (int i = 1; i < inputArray.Length; i++)
                    {
                        if (inputArray[i] > maximumNo)
                        {
                            maximumNo = inputArray[i];
                        }
                    }
                    return Ok(maximumNo);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost, Route("api/smalllestNumber")]
        public ActionResult SmallestNumberLogic(ArraysDemo1 smalllestNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[] inputArray = smalllestNumber.Array;
                    int minimumNumber = inputArray[0];
                    for (int i = 1; i < inputArray.Length; i++)
                    {
                        if (inputArray[i] < minimumNumber)
                        {
                            minimumNumber = inputArray[i];
                        }
                    }
                    return Ok(minimumNumber);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost, Route("api/twoSumArray")]
        public ActionResult ToFindSum(ArraysDemo3 twoSumArray)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? inputArray = twoSumArray.Array; int[] result = new int[2]; for (int i = 0; i < inputArray?.Length; i++)
                {
                    for (int j = i + 1; j < inputArray?.Length; j++)
                    {
                        if (inputArray[i] + inputArray[j] == twoSumArray.Target)
                        {
                            result[0] = i;
                            result[1] = j;
                        }
                    }
                }
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("api/ConvertingTempreture")]
        public ActionResult ConvertTempretureLogic(ArraysDemo4 ConvertTempreture)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                double inputKelvin = ConvertTempreture.Kelvin;
                double inputFahrenheit = ConvertTempreture.Fahrenheit;

                double[] result1 = Tempreture(inputKelvin);
                return Ok(result1);

                //double[] result2 = Tempreture(inputFahrenheit);
                //return Ok(result1);

            }

            catch (Exception)
            {
                throw;
            }
        }


        static double[] Tempreture(double celsius)
        {
            double Kelvin = celsius + 273.15D;
            double Fahrenheit = (celsius * 1.80) + 32D;

            double[] output = new double[2];
            output[0] = Math.Round(Kelvin, 5);
            output[1] = Math.Round(Fahrenheit, 5);
            return output;
        }
    }

}



