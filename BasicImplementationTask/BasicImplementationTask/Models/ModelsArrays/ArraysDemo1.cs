﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsArrays
{
    public class ArraysDemo1
    {
        [Required]
        public int[] Array { get; set; }
    }
}
