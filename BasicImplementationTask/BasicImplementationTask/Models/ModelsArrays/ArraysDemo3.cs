﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsArrays
{
    public class ArraysDemo3
    {
        [Required]
        public int[]? Array { get; set; }
        [Required]
        public int Target { get; set; }
    }
}
