﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsWhileLoop
{
    public class WhileLoop1
    {
        [Required]
        public int Number { get; set; }
    }
}
