﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsWhileLoop
{
    public class WhileLoop2
    {
        [Required]
        public int Number { get; set; }
    }
}
