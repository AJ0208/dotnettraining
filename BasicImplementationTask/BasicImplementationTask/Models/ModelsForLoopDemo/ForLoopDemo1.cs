﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsForLoopDemo
{
    public class ForLoopDemo1
    {
        [Required] 
        public string? Name { get; set; }
    }
}
