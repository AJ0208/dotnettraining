﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsSwitchCase
{
    public class SwitchCase1
    {
        [Required]
        public int Day { get; set; }
    }
}
