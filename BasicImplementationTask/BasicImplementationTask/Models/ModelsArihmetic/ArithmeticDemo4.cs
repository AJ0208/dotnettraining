﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsArihmetic
{
    public class ArithmeticDemo4
    {
        [Required]
        public int Number1 { get; set; }
        public int Number2 { get; set; }
    }
}
