﻿using System.ComponentModel.DataAnnotations;

namespace BasicImplementationTask.Models.ModelsArihmetic
{
    public class ArithmeticDemo2
    {
        [Required]
        public int Number1 { get; set; }
        public int Number2 { get; set; }
    }
}
